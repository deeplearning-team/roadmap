This Team
===

Packages are actually maintained in science team https://salsa.debian.org/science-team .
This team can be used as a temporary place for putting packaging works.

Team Goal
===

Debian Deep Learning Team maintains some deep learning related frameworks and applications
under Debian Science Team. The team itself is currently used as a temporary
workspace for the members.

We don't aim at providing the cutting edge research platform for researchers.
Full free software stack suffers from computational performance compared to
non-free implementations. Inevitably the user have to use upstream provided
binary packages or compile the softwaree by him/herself with non-free libraries.